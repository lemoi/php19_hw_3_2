<?php

/* Домашнее задание к лекции 3.2 «Наследование и интерфейсы»

    1. Распишите своё понимание полиморфизма и наследования своими словами. Представьте, что вас спрашивают на собеседовании.
    2. Своими словами распишите отличие интерфейсов и абстрактных классов. В чём отличие? Когда лучше использовать одно, когда другое?
    3. Для всех объектов, которые вы делали в прошлом ДЗ, придумайте, что могло бы быть суперклассом? (Необходимо написать код).
    4. Создайте интерфейсы для всех объектов, которые у вас были, и имплементируйте их.

Дополнительное задание:

    1. Создайте базовый класс продукта по аналогии с рассмотренным в примерах.
    2. Создайте три любых типа продукта (класса), отличных от приведенных в лекции в разных категориях.
    3. Все продукты, кроме одного, имеют 10-процентную скидку и их цена должна выводиться с ней.
    4. Один тип продукта имеет скидку только в том случае, если его вес больше 10 килограмм.
    5. Доставка на все продукты = 250 рублей, но если на продукт была скидка, то 300 рублей.
    6. Используйте примеси, интерфейсы или абстрактные классы в решении задачи.
*/

/* Домашнее задание к лекции 3.1 «Классы и объекты»

1. Распишите своё понимание инкапсуляции. Представьте, что вас спрашивают на собеседовании.
2. Сформулируйте своими словами в чём плюсы объектов, а в чём минусы?
3. Опишите 5 классов и создайте по 2 объекта каждого класса — Машина, Телевизор, Шариковая ручка, Утка, Товар.
    Классы должны содержать свойства и методы. Все в одном файле.

Дополнительное задание:

1. Создайте класс новостей для сайта.
2. Реализуйте страницу, на которой вы будете эти новости выводить, используя только методы класса (к свойствам обращаться нельзя).
3. (Необязательно) Если окажется слишком просто, реализуйте класс комментариев и создайте метод getComments в классе новостей,
    в который будете передавать объект с комментариями, который будет также их выводить.
*/

/* Ответ, своими словами, к заданию 3.2:

1. Наследование - возможность использовать свойства и/или методы одного класса (родительского) в других классах (дочерних), без необходимости их повторного описания.
   Полиморфизм - возможность переопределения реализации методов родительского класса в дочерних классах.

2. Интерфейс может содержать только определение методов (без реализации) и констант. Класс реализующий интерфес должен определить все методы, так как они описаны в интерфейсе.
   В одном классе можно реализовать несколько интерфейсов. При этом необходимо следить, чтобы одноименные методы в разных интерфейсах былы определены одинаково.
   (до PHP 5.3.9 было вообще недопустимо иметь одинаковый метод в нескольких интерфейсах)

   Абстрактный класс может иметь абстрактные методы (без реализации) и обычные, и нет органичений на определение свойств.

   Использование интерфейсов и абстрактых классов помогает избежать ошибок, неполной реализации каких-либо методов в наследующих/реализующих классах.
   Что может пригодиться, когда разные дочерние классы должны иметь одинаковый метод, но его реализация у всех разная.

3. Суперклассы:
*/

error_reporting(E_ALL);
ini_set('display_errors', 1);

class Product // Изделие
{
    protected $type;
    protected $maker;
    protected $name;

    public function __construct($name, $maker, $type)
    {
        $this->name = $name;
        $this->maker = $maker;
        $this->type = $type;
    }

    public function show()
    {
        echo "{$this->name} ({$this->type}) произведен: {$this->maker}";
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMaker()
    {
        return $this->maker;
    }
}

class Car extends Product // Машина
{
    private $color = 'Белый';
    private $speed = 0;

    public function __construct($name, $maker)
    {
        parent::__construct($name, $maker, 'машина');
    }

    public function drive($speed)
    {
        $this->speed = $speed;
    }

    public function stop()
    {
        $this->speed = 0;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color)
    {
        $this->color = $color;
        return $this;
    }
}

class Television extends Product // Телевизор
{
    private $diagonal;
    private $work = false;

    public function __construct($name, $maker)
    {
        parent::__construct($name, $maker, 'телевизор');
    }

    public function setDiagonal($diagonal)
    {
        $this->diagonal = $diagonal;
        return $this;
    }

    public function getDiagonal()
    {
        return $this->diagonal;
    }

    public function on()
    {
        $this->work = true;
    }

    public function off()
    {
        $this->work = false;
    }
}

class Pen extends Product // Ручка
{
    private $color;
    private $automatic;

    public function __construct($name, $maker, $automatic = false, $color = 'Синий')
    {
        parent::__construct($name, $maker, 'ручка');
        $this->automatic = (boolean)$automatic;
        $this->color = $color;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function replaceRod($color)
    {
        $this->color = $color;
        return $this;
    }
}

class Bird // Птица
{
    protected $gender;
    protected $kind;
    protected $name;
    protected $birthday;

    public function __construct($name, $gender, $birthday)
    {
        $this->name = $name;
        $this->gender = $gender;
        $this->birthday = $birthday;
    }

    public function setKind($kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind()
    {
        return $this->kind;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }
}

class Duck extends Bird // Утка
{
    private $act = 'Выглядит как утка';

    public function __construct($name, $gender, $birthday)
    {
        parent::__construct($name, $gender, $birthday);
        $this->kind = 'утка';
    }

    public function quack()
    {
        $this->act = 'Крякает как утка';
        echo 'Кря-кря';
    }

    public function go()
    {
        $this->act = 'Ходит как утка';
    }

    public function sleep()
    {
        $this->act = 'Спит как утка';
    }

    public function fly()
    {
        $this->act = 'Летит как утка';
    }

    public function swim()
    {
        $this->act = 'Плывет как утка';
    }

    public function look()
    {
        return $this->act;
    }
}

class Goods extends Product // Товар
{
    public $category;
    private $price = 0;
    private $weight;
    private $discount = 0;

    public function __construct($name, $maker, $price)
    {
        parent::__construct($name, $maker, 'товар');
        $this->price = $price;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getCost()
    {
        return round($this->price * (1 - $this->discount / 100));
    }

    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function show()
    {
        echo $this->getName() . '. Cтоит: ' . $this->getCost();
    }
}

//    $goods = new Goods('Смартфон Xiaomi Redmi 4X 32GB', 'Xiaomi', 9900);
//    $goods->setCategory('Телефон')->setDiscount(10);
//    $goods->show();

// 4. Интерфейсы

interface ProductInterface
{
    public function __construct($name, $maker, $type);
    public function show();
    public function getName();
    public function getType();
    public function getMaker();
}

class ProductI implements ProductInterface
{
    protected $type;
    protected $maker;
    protected $name;

    public function __construct($name, $maker, $type)
    {
        $this->name = $name;
        $this->maker = $maker;
        $this->type = $type;
    }

    public function show()
    {
        echo "{$this->name} ({$this->type}) произведен: {$this->maker}";
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMaker()
    {
        return $this->maker;
    }
}

interface CarInterface
{
    public function __construct($name, $maker);
    public function show();
    public function getName();
    public function getType();
    public function getMaker();
    public function drive($speed);
    public function stop();
    public function getColor(): string;
    public function setColor(string $color);
}

class CarI implements CarInterface // Машина
{
    protected $type;
    protected $maker;
    protected $name;
    private $color = 'Белый';
    private $speed = 0;

    public function __construct($name, $maker)
    {
        $this->name = $name;
        $this->maker = $maker;
        $this->type = 'машина';
    }

    public function show()
    {
        echo "{$this->name} ({$this->type}) произведен: {$this->maker}";
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMaker()
    {
        return $this->maker;
    }

    public function drive($speed)
    {
        $this->speed = $speed;
    }

    public function stop()
    {
        $this->speed = 0;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color)
    {
        $this->color = $color;
        return $this;
    }
}

interface TelevisionInterface
{
    public function __construct($name, $maker);
    public function show();
    public function getName();
    public function getType();
    public function getMaker();
    public function setDiagonal($diagonal);
    public function getDiagonal();
    public function on();
    public function off();
}

class TelevisionI implements TelevisionInterface // Телевизор
{
    protected $type;
    protected $maker;
    protected $name;
    private $diagonal;
    private $work = false;

    public function __construct($name, $maker)
    {
        $this->name = $name;
        $this->maker = $maker;
        $this->type = 'телевизор';
    }

    public function show()
    {
        echo "{$this->name} ({$this->type}) произведен: {$this->maker}";
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMaker()
    {
        return $this->maker;
    }

    public function setDiagonal($diagonal)
    {
        $this->diagonal = $diagonal;
        return $this;
    }

    public function getDiagonal()
    {
        return $this->diagonal;
    }

    public function on()
    {
        $this->work = true;
    }

    public function off()
    {
        $this->work = false;
    }
}

interface PenInterface
{
    public function __construct($name, $maker, $automatic, $color);
    public function show();
    public function getName();
    public function getType();
    public function getMaker();
}

class PenI implements PenInterface // Ручка
{
    protected $type;
    protected $maker;
    protected $name;
    private $color;
    private $automatic;

    public function __construct($name, $maker, $automatic = false, $color = 'Синий')
    {
        $this->name = $name;
        $this->maker = $maker;
        $this->type = 'ручка';
        $this->automatic = (boolean)$automatic;
        $this->color = $color;
    }

    public function show()
    {
        echo "{$this->name} ({$this->type}) произведен: {$this->maker}";
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMaker()
    {
        return $this->maker;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function replaceRod($color)
    {
        $this->color = $color;
        return $this;
    }
}

interface BirdInterface
{
    public function __construct($name, $gender, $birthday);
    public function setKind($kind);
    public function getKind();
    public function getName();
    public function getGender();
    public function getBirthday();
}

class BirdI implements BirdInterface
{
    protected $gender;
    protected $kind;
    protected $name;
    protected $birthday;

    public function __construct($name, $gender, $birthday)
    {
        $this->name = $name;
        $this->gender = $gender;
        $this->birthday = $birthday;
    }

    public function setKind($kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind()
    {
        return $this->kind;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }
}

interface DuckInterface
{
    public function __construct($name, $gender, $birthday);
    public function setKind($kind);
    public function getKind();
    public function getName();
    public function getGender();
    public function getBirthday();
    public function quack();
    public function go();
    public function sleep();
    public function fly();
    public function swim();
    public function look();
}

class DuckI implements DuckInterface // Утка
{
    protected $gender;
    protected $kind;
    protected $name;
    protected $birthday;
    private $act = 'Выглядит как утка';

    public function __construct($name, $gender, $birthday)
    {
        $this->name = $name;
        $this->gender = $gender;
        $this->birthday = $birthday;
        $this->kind = 'утка';
    }

    public function setKind($kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind()
    {
        return $this->kind;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function quack()
    {
        $this->act = 'Крякает как утка';
        echo 'Кря-кря';
    }

    public function go()
    {
        $this->act = 'Ходит как утка';
    }

    public function sleep()
    {
        $this->act = 'Спит как утка';
    }

    public function fly()
    {
        $this->act = 'Летит как утка';
    }

    public function swim()
    {
        $this->act = 'Плывет как утка';
    }

    public function look()
    {
        return $this->act;
    }
}

interface GoodsInterface
{
    public function __construct($name, $maker, $type);
    public function show();
    public function getName();
    public function getType();
    public function getMaker();
    public function setDiscount($discount);
    public function getDiscount();
    public function getPrice();
    public function getCost();
    public function setCategory($category);
    public function getCategory();
    public function setWeight($weight);
    public function getWeight();
}

class GoodsI implements GoodsInterface // Товар
{
    protected $type;
    protected $maker;
    protected $name;
    public $category;
    private $price = 0;
    private $weight;
    private $discount = 0;

    public function __construct($name, $maker, $price)
    {
        $this->name = $name;
        $this->maker = $maker;
        $this->type = 'товар';
        $this->price = $price;
    }

    public function show()
    {
        echo $this->getName() . '. Cтоит: ' . $this->getCost();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMaker()
    {
        return $this->maker;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getCost()
    {
        return round($this->price * (1 - $this->discount / 100));
    }

    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    public function getWeight()
    {
        return $this->weight;
    }
}

// Дополнительное задание

/*
1. Создайте базовый класс продукта по аналогии с рассмотренным в примерах.
2. Создайте три любых типа продукта (класса), отличных от приведенных в лекции в разных категориях.
3. Все продукты, кроме одного, имеют 10-процентную скидку и их цена должна выводиться с ней.
4. Один тип продукта имеет скидку только в том случае, если его вес больше 10 килограмм.
5. Доставка на все продукты = 250 рублей, но если на продукт была скидка, то 300 рублей.
6. Используйте примеси, интерфейсы или абстрактные классы в решении задачи.
*/

interface Discount
{
    public function getDiscount();

    public function setDiscount(int $discount);
}

trait DiscountTrait
{
    private $discount = 10;

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount(int $discount)
    {
        $this->discount = $discount;
        return $this;
    }
}

class BaseProduct
{
    private $name;
    private $price;
    private $priceDelivery = 250;

    public function __construct($name, $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCost()
    {
        return $this->price;
    }

    public function getPriceDelivery()
    {
        return $this->priceDelivery;
    }

    public function setPriceDelivery(int $priceDelivery)
    {
        $this->priceDelivery = $priceDelivery;
        return $this;
    }

    public function show()
    {
        echo $this->name . '. Cтоит: ' . $this->price . ' (со скидкой: ' . $this->getCost() . '). Доставка: ' . $this->getPriceDelivery();
    }
}

class Fridge extends BaseProduct
{
    // товар, который по заданию, вообще не имеет скидки
}

class Socks extends BaseProduct implements Discount
{
    // товар, на который скидка действует всегда
    use DiscountTrait;

    public function getCost()
    {
        return parent::getCost() * (100 - $this->getDiscount()) / 100;
    }

    public function getPriceDelivery()
    {
        $priceDelivery = parent::getPriceDelivery();
        return empty($this->getDiscount()) ? $priceDelivery : 300;
    }
}

class Furniture extends BaseProduct implements Discount
{
    // товар, действие скидки на который работает в зависимости от веса
    use DiscountTrait;

    protected $weight;

    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getDiscount()
    {
        return $this->weight > 10 ? $this->discount : 0;
    }

    public function getCost()
    {
        return parent::getCost() * (100 - $this->getDiscount()) / 100;
    }

    public function getPriceDelivery()
    {
        $priceDelivery = parent::getPriceDelivery();
        return empty($this->getDiscount()) ? $priceDelivery : 300;
    }

    public function show()
    {
        parent::show();
        echo '. Весит: ' . $this->weight;
    }
}

$socks = new Socks('Носки черные', 200);
$fridge = new Fridge('Холодильник Bosh', 20000);
$table = new Furniture('Стол письменный', 8000);
$table->setWeight(8);
$wardrobe = new Furniture('Стенка', 25000);
$wardrobe->setWeight(30);

$socks->show();
echo '<br>';
$fridge->show();
echo '<br>';
$table->show();
echo '<br>';
$wardrobe->show();
echo '<br>';
